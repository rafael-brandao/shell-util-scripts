{
  description = "Several shell utility functions.";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/release-21.05";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, ... }@inputs:
    with inputs;
    (flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ self.overlay ];
          config = { allowUnsupportedSystem = true; };
        };

      in rec {
        packages = {
          inherit (pkgs)
            shell-util-containers shell-util-functions shell-util-hardware
            shell-util-nix;
        };

        devShell = with pkgs;
          mkShell {
            packages = [
              bashInteractive
              entr
              shell-util-containers
              shell-util-functions
              shell-util-hardware
              shell-util-nix
            ];

            shellHook = ''
              source shell-util-functions
              source shell-util-containers
              source shell-util-hardware
              source shell-util-nix
            '';
          };

      })) // {
        overlay = import ./scripts;
      };
}
