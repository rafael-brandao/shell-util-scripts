# shellcheck shell=bash

# Allow mounting unprivileged containers
# [ -n "${__UNSHARED-}" ] || {
#   export __UNSHARED=1
#   buildah unshare "$0"
#   exit
# }

[ -z "${__SHELL_UTIL_CONTAINERS_SOURCED-}" ] || return 0
__SHELL_UTIL_CONTAINERS_SOURCED=1

# shellcheck source=functions.sh
\. "shell-util-functions"
_require_script_sourcing

_get_image_id() {
  [ -n "${1-}" ] || {
    _error "${BASH_SOURCE[0]}: line $LINENO: A filter must be passed to '_get_image_id'"
    return 201
  }
  buildah images --noheading --no-trunc --quiet --filter="$1"
}

_remove_network() {
  [ -n "${1-}" ] || {
    _error "${BASH_SOURCE[0]}: line $LINENO: A filter must be passed to '_remove_network'"
    return 201
  }
  podman network ls \
    --filter "name=$1" --noheading --format '{{.ID}}' |
    xargs --no-run-if-empty podman network rm
}

_execute_build() {
  local upstream_digest_label='upstream-digest'
  local container_name
  local container_registry
  local current_image_id
  local image_tag
  local local_url
  local remote_image_digest
  local tls_verify

  __validate_env() {
    [ -n "${IMAGE_REPOSITORY-}" ] || _fail 1 "Variable 'IMAGE_REPOSITORY' must be set before calling '_execute_build'"
    [ -n "${BUILD_IMAGE_FN-}" ] || _fail 2 "Variable 'BUILD_IMAGE_FN' must be set before calling '_execute_build'"
  }

  __set_build_inputs() {
    image_tag="${IMAGE_TAG-latest}"
    container_name="$(echo "$IMAGE_REPOSITORY" | tr / -)-$(cut -c 25- /proc/sys/kernel/random/uuid)"
    container_registry="${CONTAINER_REGISTRY-docker.io}"
    local_url="${LOCAL_URL-$IMAGE_REPOSITORY:$image_tag}"
    remote_image_url="$container_registry/$IMAGE_REPOSITORY:$image_tag"
    old_image_id=$(_get_image_id "reference=localhost/$local_url")

    tls_verify=$(
      if [ -z ${CONTAINER_TLS_VERIFY+x} ] ||
        { [ -n "${CONTAINER_TLS_VERIFY-}" ] && [ "${CONTAINER_TLS_VERIFY,,}" != "false" ]; }; then
        echo "true"
      else
        echo "false"
      fi
    )
  }

  __on_exit() {
    # Remove remote image
    __exclude_remote_image() {
      if [ -n "${KEEP_REMOTE_IMAGE-}" ] && [ "${KEEP_REMOTE_IMAGE,,}" = 'true' ]; then
        return
      fi
      local remote_image_id

      [ -z "${remote_image_url-}" ] || remote_image_id=$(_get_image_id "reference=$remote_image_url")
      [ -z "${remote_image_id-}" ] || {
        _info "Removing remote image '$remote_image_url'"
        buildah rmi "$remote_image_id" || true
      }
    }
    __exclude_remote_image
  }

  __check_cache() {
    if [ -n "${SKIP_CACHE-}" ] && [ "${SKIP_CACHE,,}" = 'false' ]; then
      false
    else
      local local_image_digest
      local_image_digest=$(
        skopeo inspect \
          --format "{{index .Labels \"$upstream_digest_label\"}}" \
          containers-storage:localhost/"$local_url" 2>/dev/null || true
      )
      [ "$remote_image_digest" = "$local_image_digest" ]
    fi
  }

  __pre_start() {
    _info 'Getting remote image digest'
    remote_image_digest=$(
      skopeo inspect \
        --tls-verify="$tls_verify" \
        --format "{{.Digest}}" \
        docker://"$remote_image_url"
    )

    [ -z "${PRE_START_FN-}" ] || "$PRE_START_FN"
  }

  __build_image() {

    __post_build_image() {
      # Remove buildah working container
      [ -z "${container_name-}" ] || [ -n "${KEEP_CONTAINER-}" ] || {
        _info "Removing buildah container '$container_name'"
        buildah containers --noheading --quiet --filter=name="$container_name" | xargs buildah rm
      }

      # Remove old image
      [ -z "${old_image_id-}" ] || [ -z "${current_image_id-}" ] || [ "${current_image_id}" = "$old_image_id" ] || {
        _info "Removing previous image pointing to 'localhost/$local_url'"
        buildah rmi "$old_image_id" || true
      }
    }

    _info "Checking if image 'localhost/$local_url' is up to date"
    if __check_cache; then
      _info "Image 'localhost/$local_url' is up to date"
    else
      _info "Image 'localhost/$local_url' is not up to date"

      trap '__on_exit' EXIT
      CONTAINER=$(buildah from --name "$container_name" --tls-verify="$tls_verify" --pull "$remote_image_url")
      CONTAINER_MOUNT=$(buildah mount "$CONTAINER")

      # execute build function
      export CONTAINER
      export CONTAINER_MOUNT
      "$BUILD_IMAGE_FN"

      # trap '__on_return $? $LINENO' RETURN
      [ -z "${CONTAINER_MOUNT-}" ] || buildah unmount "$CONTAINER"
      buildah config --label "$upstream_digest_label=$remote_image_digest" "$CONTAINER"
      buildah commit "$CONTAINER" "$local_url"
      __post_build_image
    fi
  }

  __validate_env
  __set_build_inputs
  __pre_start
  __build_image
}
