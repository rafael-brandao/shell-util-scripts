# shellcheck shell=sh

[ -z "${__SHELL_UTIL_HARDWARE_SOURCED-}" ] || return 0
__SHELL_UTIL_HARDWARE_SOURCED=1

# shellcheck source=functions.sh
\. "shell-util-functions"
_require_script_sourcing

_logical_cpu_count() {
  if [ "$(uname)" != 'Darwin' ]; then
    lscpu --parse |
      rg --invert-match '^#' |
      wc --lines
  else
    sysctl --values hw.logicalcpu_max
  fi
}

_physical_cpu_count() {
  if [ "$(uname)" != 'Darwin' ]; then
    lscpu --parse |
      rg --invert-match '^#' |
      sort --unique --field-separator , --key 2,4 |
      wc --lines
  else
    sysctl --values hw.physicalcpu_max
  fi
}
