#! /usr/bin/env bash

[ -z "${__SHELL_UTIL_NIX_SOURCED-}" ] || return 0
__SHELL_UTIL_NIX_SOURCED=1

# shellcheck source=functions.sh
\. 'shell-util-functions'
_require_script_sourcing

# shellcheck source=hardware.sh
\. 'shell-util-hardware'

_generate_vscode_extensions() {

  declare temp_working_dir

  __cleanup() {
    # print error code when this script fails
    [ "$1" -eq 0 ] || _warn "_generate_vscode_extensions exit code: $1"

    # remove temp working directory in background
    ([ ! -d "${temp_working_dir:-}" ] || rm --force --recursive "${temp_working_dir}") &
  }

  __get_vsixpkg() {
    declare extension_name extension_owner extension_version extension_hash extension_temp_file url

    extension_owner="${1%.*}"
    extension_name="${1#*.}"

    extension_temp_file="$temp_working_dir/$1/extension.zip"
    mkdir "$(dirname "$extension_temp_file")"

    url="https://$extension_owner.gallery.vsassets.io/_apis/public/gallery/publisher/$extension_owner/extension/$extension_name/latest/assetbyname/Microsoft.VisualStudio.Services.VSIXPackage"

    # Quietly but delicately curl down the file, blowing up at the first sign of trouble.
    curl --silent --show-error --fail -X GET -o "$extension_temp_file" "$url"

    # Unpack the file we need to stdout then pull out the version
    extension_version=$(jq -r '.version' <(unzip -cq "$extension_temp_file" "extension/package.json"))
    extension_hash=$(nix hash file --base32 --type sha256 "$extension_temp_file")

    cat <<-EOF
    $1 = extensionFromVscodeMarketplace {
      name = "$extension_name";
      publisher = "$extension_owner";
      version = "$extension_version";
      sha256 = "$extension_hash";
    };

EOF
  }

  __print_header() {
    cat <<-EOF
final: prev:

let
  inherit (final.lib.attrsets) recursiveUpdate;
  inherit (final.vscode-utils) extensionFromVscodeMarketplace;

in {
  vscode-extensions = recursiveUpdate prev.vscode-extensions {

EOF
  }

  __print_footer() {
    cat <<-EOF
  };
}
EOF
  }

  __read_json_file() {
    __sanitize_json() {
      sed --regexp-extended 's/[\t\ ]*\/\/.+$//g' | sed '/^[\t\ ]*\/\//d' | sed '/^[[:space:]]*$/d'
    }
    __sanitize_json <"$1" | jq '.[]' | sed 's/"//g'
  }

  __distribute_tasks_to_workers() {

    __get_number_of_workers() {
      declare -i max_workers logical_cores
      max_workers="${MAX_WORKERS-8}"
      logical_cores="$(_logical_cpu_count)"
      [ "$logical_cores" -gt "$max_workers" ] && echo "$max_workers" || echo "$logical_cores"
    }

    declare -i number_of_tasks number_of_workers tasks_per_worker remainder_tasks
    declare -n _workers_array="$2"

    number_of_tasks="$1"
    number_of_workers=$(__get_number_of_workers)

    tasks_per_worker=$((number_of_tasks / number_of_workers))
    remainder_tasks=$((number_of_tasks % number_of_workers))

    while [ "$number_of_workers" -gt 0 ]; do
      if [ "$remainder_tasks" -gt 0 ]; then
        _workers_array+=($((tasks_per_worker + 1)))
        remainder_tasks=$((remainder_tasks - 1))
      else
        _workers_array+=($((tasks_per_worker)))
      fi
      number_of_workers=$((number_of_workers - 1))
    done
  }

  __get_worker_file() {
    declare -i worker_index="$1"
    echo "$temp_working_dir/worker_$worker_index.result"
  }

  __proccess_tasks() {
    declare -i first_index="$1" last_index="$2"
    declare extensions="$3" worker_index="$4"

    declare worker_name="shell-util-nix-$worker_index"

    declare worker_file
    declare -a extensions_array

    mapfile -t extensions_array < <(
      echo "$extensions" |
        awk \
          --assign first="$first_index" \
          --assign last="$last_index" \
          'NR >= first && NR <= last { print $1 }'
    )

    worker_file=$(__get_worker_file "$worker_index")
    touch "$worker_file"

    for extension in "${extensions_array[@]}"; do
      _info "$worker_name: _generate_vscode_extensions processing extension '$extension'"
      __get_vsixpkg "$extension" >>"$worker_file"
    done
  }

  __main() {
    (
      set -Eeuo pipefail

      declare json_extensions_file raw_extensions result_file
      declare -a workers_array

      json_extensions_file="$1"
      result_file="${2-"$(dirname "$json_extensions_file")/default.nix"}"

      # make temp working directory
      temp_working_dir="$(mktemp --directory -t "${FUNCNAME[1]}_XXXXXXXX")"
      trap '__cleanup $?' EXIT

      # read extensions file
      raw_extensions=$(__read_json_file "$json_extensions_file")

      __distribute_tasks_to_workers "$(echo "$raw_extensions" | wc --lines)" workers_array

      declare -i first_index last_index
      first_index=1
      last_index=0

      for worker_index in "${!workers_array[@]}"; do
        last_index=$((last_index + "${workers_array[worker_index]}"))

        (__proccess_tasks "$first_index" "$last_index" "$raw_extensions" "$worker_index") &

        first_index=$((last_index + 1))
      done
      wait

      # Proccess results
      __print_header >"$result_file"
      for worker_index in "${!workers_array[@]}"; do
        cat "$(__get_worker_file "$worker_index")" >>"$result_file"
      done
      __print_footer >>"$result_file"
    )
  }

  __main "$@"
}
