# shellcheck shell=bash

[ -z "${__SHELL_UTIL_FUNCTIONS_SOURCED-}" ] || return 0
__SHELL_UTIL_FUNCTIONS_SOURCED=1

#-------------------------------------------------------------------------------------------------
# Setup colors, but only if connected to a terminal
#-------------------------------------------------------------------------------------------------
if [ -t 1 ]; then
  # BLUE=$(printf '\033[34m')
  # DARK_GRAY=$(printf '\033[90m')
  # GRAY=$(printf '\033[37m')
  GREEN=$(printf '\033[32m')
  RED=$(printf '\033[31m')
  YELLOW=$(printf '\033[33m')
  BOLD=$(printf '\033[1m')
  RESET=$(printf '\033[m')
else
  # BLUE=""
  # DARK_GRAY=""
  # GRAY=""
  GREEN=""
  RED=""
  YELLOW=""
  BOLD=""
  RESET=""
fi

#-------------------------------------------------------------------------------------------------
# Common Functions
#-------------------------------------------------------------------------------------------------
_info() {
  printf >&2 '%s\n' "$*"
}

_success() {
  printf >&2 '%s\n' "${BOLD}${GREEN}$*${RESET}"
}

_error() {
  printf >&2 '%s\n' "${RED}$*${RESET}"
}

_warn() {
  printf >&2 '%s\n' "${BOLD}${YELLOW}$*${RESET}"
}

_underline() {
  echo "$(printf '\033[4m')$*$(printf '\033[24m')"
}

_fail() {
  local error_code error_message

  if [ "$#" -gt 1 ]; then
    error_code="$1"
    error_message="$2"
  else
    error_code=1
    error_message="$1"
  fi

  _error "$error_message"
  exit $((error_code))
}

_require_script_sourcing() {
  local lineno script

  if [ "$#" -gt 1 ]; then
    script="$1"
    lineno="$2"
  else
    script="${BASH_SOURCE[1]}"
    lineno="${1-}"
  fi

  [ "$script" != "$0" ] || {
    if [ -n "$lineno" ]; then
      _fail 255 "$script: $lineno: $(basename "$script") is supposed to be sourced"
    else
      _fail 255 "$script $(basename "$script") is supposed to be sourced"
    fi
  }
}

_require_script_execution() {
  local lineno script

  if [ "$#" -gt 1 ]; then
    script="$1"
    lineno="$2"
  else
    script="${BASH_SOURCE[1]}"
    lineno="${1-}"
  fi

  [ "$script" == "$0" ] || {
    if [ -n "$lineno" ]; then
      _fail 254 "$script: $lineno: $(basename "$script") is supposed to be executed"
    else
      _fail 254 "$script $(basename "$script") is supposed to be executed"
    fi
  }
}

_git_repo_path() {
  local repo_path script
  script="${BASH_SOURCE[1]-$(pwd)}"

  repo_path=$([ -f "$script" ] && dirname "$(realpath "$script")" || echo "$script")

  while [ ! -d "$repo_path/.git" ]; do
    repo_path=$(cd "$repo_path/.." || _fail "$script: could not cd to parent directory"; pwd)
    if [ "$repo_path" = '/' ]; then
      _fail "$script: no parent git repository was found"
    fi
  done

  echo "$repo_path"
}

_require_script_sourcing "${BASH_SOURCE[0]}" "$LINENO"
