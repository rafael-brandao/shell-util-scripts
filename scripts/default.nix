self: super:

let
  mkPackageScript = { description, deps ? [ ], script }:
    super.callPackage ({ lib, stdenvNoCC, bash }:
      stdenvNoCC.mkDerivation {
        name = "shell-util-${script}";
        src = ./. + "/${script}.sh";

        doCheck = true;
        dontPatchShebangs = true;
        checkInputs = [ bash ];

        unpackPhase = ":";
        propagatedBuildInputs = deps;

        installPhase = ''
          mkdir -p $out/bin
          cp $src $out/bin/shell-util-${script}
        '';

        meta = with lib; {
          inherit description;
          license = licenses.bsd3;
          platforms = platforms.all;
        };

      }) { };

in rec {

  shell-util-containers = mkPackageScript {
    script = "containers";
    description =
      "shell functions to manipulate containers using tools like buidah and skopeo";
    deps = with super.pkgs; [ buildah podman shell-util-functions skopeo ];
  };

  shell-util-functions = mkPackageScript {
    script = "functions";
    description = "shell utilitiy functions to boost productivity";
    deps = with super.pkgs; [ coreutils ];
  };

  shell-util-hardware = mkPackageScript {
    script = "hardware";
    description = "shell utilitiy functions related to hardware info";
    deps = with super.pkgs; [
      coreutils
      ripgrep
      shell-util-functions
      util-linux
    ];
  };

  shell-util-nix = mkPackageScript {
    script = "nix";
    description = "shell functions to generate nix files";
    deps = with super.pkgs; [
      coreutils
      curl
      jq
      nixfmt
      shell-util-functions
      shell-util-hardware
      unzip
    ];
  };

}
